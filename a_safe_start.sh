#!/bin/bash

# GPL 2021 Lars Falk-Petersen <dev@falkp.no>

# Check that apache can be safely restarted
# * Check that log dir exists
# * Check that config files exist and validates
# * Check that certificates exists and validates
# * Check that keys exists and validates
# * Check that certificates and keys belong

# TODO
# * Check files other that sites-enabled (i.e. conf-enabled)
# * Take apache dir as argument?

set -u # stop on empty vars
#set -e # Die on any error
set -o pipefail # Die if a command in a pipe fails

verbose=""
reload=""
apacheconfdir="/etc/apache2/sites-enabled/"

function parse_args {
    local OPTIND
    while getopts "vhra:" opt; do
        case ${opt} in
        v )
        verbose="1"
        ;;
        h )
        echo -e "Usage: $0 [-v] [-r] [-h] [-a <config dir>]\n -v Verbose\n -r Reload apache if checks are OK\n -a <apache config dir>"
        exit 127
        ;;
        r )
        reload="1"
        ;;
        a )
        apacheconfdir="$OPTARG"
        [[ "x$verbose" == "x1" ]] && {
            echo "Conf dir set to $apacheconfdir";
        }
        ;;
        \? )
        echo "Invalid option: $OPTARG" 1>&2
        exit 1
        ;;
        : )
        echo "Invalid option: $OPTARG requires an argument" 1>&2
        exit 1
        ;;
    esac
    done
    shift $((OPTIND -1))
}

function check_user {
    [[ 0 -eq $(id --user) ]] || {
        echo "Error, run me as root"
        exit 1;
    }
}

function run_apachectl {
    [[ "x$verbose" == "x1" ]] && {
        echo "* Running $apachectl_bin configtest";
    }
    $apachectl_bin -t 2> /dev/null || {
        echo "Error from $apachectl_bin: $($apachectl_bin -t)"
        exit 1;
    }
}

function dirs_exists {
    # TODO parse suffix and such or
    # get log dir from apachectl -t -D DUMP_RUN_CFG
    conffile="/etc/apache2/envvars"
    confname="APACHE_LOG_DIR"
    logdir=$(grep $confname $conffile|cut -d= -f2|tr -d '$SUFFIX')
    [[ "x$verbose" == "x1" ]] && {
        echo "* Checking log dir exists";
    }
    test -d "$logdir" || {
        echo "Error reading logdir $logdir"
        exit 1;
    }
    [[ "x$verbose" == "x1" ]] && {
        echo "* Checking conf dir exists";
    }
    test -d "$apacheconfdir" || {
        echo "Error reading conf dir $apacheconfdir"
        exit 1;
    }
    [[ "x$verbose" == "x1" ]] && {
        echo "* Checking envvars exists";
    }
    test -f "$conffile" || {
        echo "Error reading apache envvars file $conffile"
        exit 1;
    }
}

function check_key {
    # Check that keyfile is valid. Check key and cert belong.

    keyfile="$1"
    certfile="$2"
    
    if ! keyresult=$( openssl rsa -in "$keyfile" -check -noout ); then
        echo "Key $keyfile invalid $keyresult"
        exit 5
    fi
    [[ "x$verbose" == "x1" ]] && {
        echo "** Keyfile $keyfile checks out"
    }

    certsum=$( openssl x509 -noout -modulus -in "$certfile" | openssl sha256 )
    keysum=$( openssl rsa -noout -modulus -in "$keyfile" | openssl sha256 )
    [[ "x$certsum" == "x" ]] && {
        echo "Error: certificate $certfile seems bad"
        exit 7;
    }
    [[ "x$verbose" == "x1" ]] && {
        echo "** Certificate $certfile checks out"
    }

    [[ "x$certsum" == "x$keysum" ]] || {
        echo "Cert $certfile and key $keyfile don't match!"
        exit 4;
    }
    [[ "x$verbose" == "x1" ]] && {
        echo "** Cert and key match";
    }

}

function loop_configs {
    # TODO check SSLCertificateChainFile if exitsts

    # List all files and symlinks that's not in a hidden dir (like .git)
    configfiles=$(find "$apacheconfdir" -not -type d -not -path "*/.*")
    for f in $configfiles; do
        [[ "x$verbose" == "x1" ]] && {
            echo "* Testing config file $f exits";
        }
        test -e "$f" || {
            echo "Error, symlink $f is broken"
            exit 1;
        }

        # Loop vhosts in file
        start_tag="<virtualhost"
        end_tag="</virtualhost>"
        ssl_tag="sslengine"
        servername_tag="servername"
        cert_tag="sslcertificatefile"
        key_tag="sslcertificatekeyfile"
        ca_tag="sslcertificatechainfile"

        sslon=""
        sslcertfile=""
        sslkeyfile=""
        sslcafile=""
        servername=""

        while IFS="" read -r p || [ -n "$p" ]; do
            if [ 0 -lt "$(echo "$p" | sed -e 's/^[[:space:]]*//'| grep -E -c "^#")" ]; then # Skip commented lines
                continue
            fi
            p="${p,,}" # Make string lowercase

            if [[ $p =~ $start_tag ]]; then
                #echo "start vhost $p"
                sslon=""
                sslcertfile=""
                sslkeyfile=""
                sslcafile=""
                servername=""
            fi

            if [[ $p =~ $end_tag ]]; then
                #echo "end vhost $servername"
                # Vhost done, run SSL tests if found
                [[ "x$sslon" == "x" ]] && continue
                [[ "x$verbose" == "x1" ]] && {
                    echo "** SSL enabled in vhost. Checking $sslkeyfile, $sslcertfile";
                }
                check_key "$sslkeyfile" "$sslcertfile";
                continue
            fi

            if [[ $p =~ $ssl_tag ]]; then
                echo "$p" | grep -si " on" > /dev/null && sslon="1"
            fi
            if [[ $p =~ $servername_tag ]]; then
                servername="$p"
            fi
            if [[ $p =~ $cert_tag ]]; then
                sslcertfile=$(echo "$p" | sed -e 's/^[[:space:]]*//'| grep -Ev "^#" |  awk '{ print $NF }' |tr -d '"')
                test -f "$sslcertfile" || {
                    echo "Error, missing $cert_tag file <$sslcertfile> from vhost $servername in file $f."
                    exit 1
                }
            fi
            if [[ $p =~ $key_tag ]]; then
                sslkeyfile=$(echo "$p" | sed -e 's/^[[:space:]]*//'| grep -Ev "^#" |  awk '{ print $NF }' |tr -d '"')
                test -f "$sslkeyfile" || {
                    echo "Error, missing $cert_tag file <$sslkeyfile> from vhost $servername in file $f."
                    exit 1
                }
            fi
            if [[ $p =~ $ca_tag ]]; then
                sslcafile=$(echo "$p" | sed -e 's/^[[:space:]]*//'| grep -Ev "^#" |  awk '{ print $NF }' |tr -d '"')
                test -f "$sslcafile" || {
                    echo "Error, missing $cert_tag file <$sslcafile> from vhost $servername in file $f."
                    exit 1
                }
            fi
        done < "$f"
    done
}

function warn_inactive_configs {
    configfiles=$(find /etc/apache2/sites-enabled -not -type d -not -path "*/.*" -not -name \*.conf)
    for f in $configfiles; do
        echo "Warning: file will not be used by Apache: $f"
    done
}

function reload_apache {
    [[ "x$reload" == "x1" ]] && {
        [[ "x$verbose" == "x1" ]] && {
            echo "# Reloading apache #";
        }
        $apachectl_bin -k graceful
        sleep 1
        check_alive
    }
}

function check_alive {
    if ! pidof -s apache2 > /dev/null ; then
        echo "Error, apache isn't running!"
        exit 1;
    fi
    [[ "x$verbose" == "x1" ]] && {
        echo "Apache restarted OK";
    }

}

# Main

parse_args "$@"

apachectl_bin=$(which apachectl) || {
    echo "Unable to find apachectl using <which apachectl>"
    exit 1;
}

check_user
run_apachectl
dirs_exists
loop_configs
warn_inactive_configs

[[ "x$verbose" == "x1" ]] && {
    echo "Preflight check OK"
}

reload_apache

exit 0
