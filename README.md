Apache Safe Start
=================

Extra checks before reloading apache configs

    Usage: ./a_safe_start.sh [-v] [-r] [-h] [-a <config dir>]
    -v Verbose
    -r Reload apache if checks are OK
    -a <apache config dir>

!
! Only tested where files in sites-enabled/ are files, not symlinks.
!

Example run
===========

    $ sudo bash ./a_safe_start.sh -v -a testdata
    Conf dir set to testdata
    * Running /usr/sbin/apachectl configtest
    * Checking log dir exists
    * Checking conf dir exists
    * Checking envvars exists
    * Testing config file testdata/nymusikk.no.conf exits
    ** SSL enabled in vhost. Checking testdata/privkey.pem, testdata/fullchain.pem
    ** Keyfile testdata/privkey.pem checks out
    ** Certificate testdata/fullchain.pem checks out
    ** Cert and key match
    ** SSL enabled in vhost. Checking testdata/privkey.pem, testdata/fullchain.pem
    ** Keyfile testdata/privkey.pem checks out
    ** Certificate testdata/fullchain.pem checks out
    ** Cert and key match
    ** SSL enabled in vhost. Checking testdata/privkey2.pem, testdata/fullchain2.pem
    ** Keyfile testdata/privkey2.pem checks out
    ** Certificate testdata/fullchain2.pem checks out
    ** Cert and key match
    * Testing config file testdata/privkey.pem exits
    * Testing config file testdata/privkey2.pem exits
    * Testing config file testdata/fullchain.pem exits
    * Testing config file testdata/fullchain2.pem exits
    Preflight check OK
    # Reloading apache #
    Apache restarted OK

TODO
====

* Speed up
